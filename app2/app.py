"""
A sample app demonstrating Stackdriver Trace
"""
import argparse
import random
import time
import socket

# [START trace_demo_imports]
from flask import Flask
from opencensus.ext.flask.flask_middleware import FlaskMiddleware
from opencensus.ext.stackdriver.trace_exporter import StackdriverExporter
from opencensus.trace import execution_context
from opencensus.trace.propagation import google_cloud_format
from opencensus.trace.samplers import AlwaysOnSampler
# [END trace_demo_imports]
import requests


app = Flask(__name__)

# [START trace_demo_middleware]
propagator = google_cloud_format.GoogleCloudFormatPropagator()


def createMiddleWare(exporter):
    # Configure a flask middleware that listens for each request and applies automatic tracing.
    # This needs to be set up before the application starts.
    middleware = FlaskMiddleware(
        app,
        exporter=exporter,
        propagator=propagator,
        sampler=AlwaysOnSampler())
    return middleware
# [END trace_demo_middleware]


@app.route('/')
def template_test():
    # Sleep for a random time to imitate a random processing time
    time.sleep(random.uniform(0, 0.3))
    # Keyword that gets passed in will be concatenated to the final output string.
    output_string = app.config['keyword']
    # If there is no endpoint, return the output string.
    url = app.config['endpoint']
    if url == "":
        return output_string, 200
    # Endpoint is the next service to send string to.
    data = {'body': output_string}
    # [START trace_context_header]
    trace_context_header = propagator.to_header(execution_context.get_opencensus_tracer().span_context)
    response = requests.get(
        url,
        params=data,
        headers={
          'X-Cloud-Trace-Context' : trace_context_header}
    )
    # [END trace_context_header]
    return response.text + app.config['keyword']


if __name__ == "__main__":
    hostn = str(socket.gethostname())
    keyw = hostn + " this is app2 words! \n "
    parser = argparse.ArgumentParser()
    parser.add_argument("--keyword",  default=keyw, help="name of the service.")
    parser.add_argument("--endpoint", default="http://app3", help="endpoint to dispatch appended string, simply respond if not set")
    args = parser.parse_args()
    app.config['keyword'] = args.keyword
    app.config['endpoint'] = args.endpoint
    # [START trace_demo_create_exporter]
    createMiddleWare(StackdriverExporter())
    # [END trace_demo_create_exporter]
    app.run(debug=True, host='0.0.0.0', port=80)
