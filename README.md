Use 
    gcloud builds submit --tag gcr.io/$PROJECT/trace1:2.0 ./app1
to create and upload docker image to GCP docker repository.

=========

1. create 2 GKE clusters
2. install ASM

   ./install_asm \
      --mode install \
      --managed \
      -p $PROJECT \
      -l $LOC \
      -n $CLUSTER -v \
      --output_dir output \
      --enable-all
3. create ns hello
4. label ns hello with asm-managed
    kubectl label namespace hello istio-injection- istio.io/rev=asm-managed --overwrite
5. create cluster1 objects in cluster1
    kubectx $CLUSTER-1
    kubectl apply -f config/cluster1/
6. create cluster2 objects in cluster2
    kubectx $CLUSTER-2
    kubectl apply -f config/cluster2/
 
